package com.nattapatsaeng.moviesearch.ui.main.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.nattapatsaeng.moviesearch.R

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}