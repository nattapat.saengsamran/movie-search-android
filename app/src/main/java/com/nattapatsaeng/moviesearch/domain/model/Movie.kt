package com.nattapatsaeng.moviesearch.domain.model

data class Movie(
    val name: String,
    val averageVote: Double,
    val releaseDate: String,
    val description: String,
    val id: Int
)
