package com.nattapatsaeng.moviesearch.domain.repository

import com.nattapatsaeng.moviesearch.data.remote.dto.MovieDto

interface MovieRepository {
    suspend fun getMovies(query: String ,page: Int): List<MovieDto>
}