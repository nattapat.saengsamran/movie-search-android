package com.nattapatsaeng.moviesearch.domain.usecases.getmovies

import com.nattapatsaeng.moviesearch.common.Resource
import com.nattapatsaeng.moviesearch.data.remote.dto.toMovie
import com.nattapatsaeng.moviesearch.domain.model.Movie
import com.nattapatsaeng.moviesearch.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class getMoviesUseCases @Inject constructor(
    private val repository: MovieRepository
) {
    operator fun invoke(query: String, page: Int): Flow<Resource<List<Movie>>> = flow {
        try {
            emit(Resource.Loading())
            val movies: List<Movie> = repository.getMovies(query, page).map { it.toMovie() }
            emit(Resource.Success(movies))
        }
        catch (e: HttpException) {
            emit(Resource.Error(e.message?: "Unknown error occur"))
        }
        catch (e: IOException) {
            emit(Resource.Error("Couldn't reach server. Check your internet connection"))
        }
    }
}