package com.nattapatsaeng.moviesearch.data.datarepository

import com.nattapatsaeng.moviesearch.data.remote.MovieApi
import com.nattapatsaeng.moviesearch.data.remote.dto.MovieDto
import com.nattapatsaeng.moviesearch.domain.repository.MovieRepository
import javax.inject.Inject

class MovieDataRepository @Inject constructor(
    private val api: MovieApi
) : MovieRepository {
    override suspend fun getMovies(query: String, page: Int): List<MovieDto> {
        return api.getMovie(query, page).results
    }
}