package com.nattapatsaeng.moviesearch.data.remote

import com.nattapatsaeng.moviesearch.common.Constants
import com.nattapatsaeng.moviesearch.data.remote.dto.MoviesDto
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface MovieApi {
    @Headers(Constants.apiKey)
    @GET("/api/movies/search")
    suspend fun getMovie( @Query("query") queryWord: String, @Query("page") page: Int): MoviesDto
}